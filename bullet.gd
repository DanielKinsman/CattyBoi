extends RigidBody2D

const DAMAGE := 2
const KNOCKBACK := 500.0


func _ready():
    $HurtBox.connect("body_entered", self, "_on_entered")


func _on_entered(other):
    # don't hurt player
    if other.has_method("hurt") and !other.has_method("process_input"):
        other.hurt(DAMAGE)
        var knockback := Vector2.ZERO
        knockback = (other.global_position - self.global_position).normalized() * KNOCKBACK
        other.velocity += knockback
        self.queue_free()
    elif other is StaticBody2D:
        self.queue_free()
