extends Node2D

const SHAKE_MAX_TRANS := Vector2(128, 128)
const SHAKE_MAX_ROLL := PI / 8.0

var target_node: NodePath
var asymptote_factor := 0.075
var translation_x_noise: OpenSimplexNoise
var translation_y_noise: OpenSimplexNoise
var rotation_noise: OpenSimplexNoise

onready var target: Vector2
onready var trauma := float(1.0)


func _ready():
    translation_x_noise = OpenSimplexNoise.new()
    translation_y_noise = OpenSimplexNoise.new()
    rotation_noise = OpenSimplexNoise.new()
    translation_x_noise.seed = randi()
    translation_y_noise.seed = randi()
    rotation_noise.seed = randi()


func _noise_time() -> float:
    return OS.get_ticks_usec() / 750.0


func _process(delta: float):
    if !target_node.is_empty():
        target = get_node(target_node).global_position

    self.global_position += (target - self.global_position) * asymptote_factor * (delta * 60)

    var shake = pow(Trauma.get_trauma(), 3)

    var shake_offset := Vector2.ZERO
    var noise_time := _noise_time()
    shake_offset.x = SHAKE_MAX_TRANS.x * shake * translation_x_noise.get_noise_1d(noise_time)
    shake_offset.y = SHAKE_MAX_TRANS.y * shake * translation_y_noise.get_noise_1d(noise_time)
    var shake_roll_offset = SHAKE_MAX_ROLL * shake * rotation_noise.get_noise_1d(noise_time)

    # roll not working?
    $Camera2D.transform = Transform2D(shake_roll_offset, shake_offset)
