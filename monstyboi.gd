extends KinematicBody2D

const FRICTION := 0.9
const ACCEL := 2000.0
const JUMP_SPEED = 2000.0
const GRAVITY = Vector2(0, 100)
const UP := Vector2(0, -1)
const DAMAGE := 1

onready var hit_points := 5
onready var velocity := Vector2.ZERO
var player = null


func _ready():
    self.add_to_group("Monsters")
    $HurtBox.connect("body_entered", self, "_body_entered_hurtbox")


func hurt(damage : int):
    if self.hit_points <= 0:
        self.queue_free()
        return

    self.hit_points -= damage
    $AudioDie.play()
    if self.hit_points <= 0:
        $AnimationPlayer.play("die")


func _physics_process(delta : float):
    if player == null:
        player = get_tree().get_nodes_in_group("Player")[0]

    var movement := Vector2.ZERO
    if self.hit_points <= 0:
        self.velocity += GRAVITY
    elif player != null:
        self.look_at(player.global_position)
        movement += player.global_position - self.global_position

    self.velocity = self.move_and_slide(
        (self.velocity * FRICTION) + (movement.normalized() * ACCEL * delta), UP)


func _body_entered_hurtbox(other):
    if other.has_method("hurt") and other != self and self.hit_points > 0:
        other.hurt(DAMAGE)
