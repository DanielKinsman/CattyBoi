extends Node2D

const ImpScene := preload("res://monstyboi.tscn")

const MONSTER_SPAWN_TIME := 7.0


onready var rng := RandomNumberGenerator.new()
onready var ImpSpawnerTimer := Timer.new()
export var is_satan_level := false


func _ready():
    $CattyBoi.is_satan_level = is_satan_level
    rng.randomize()
    self.add_child(ImpSpawnerTimer)
    ImpSpawnerTimer.connect("timeout", self, "spawn_imp")

    if !is_satan_level:
        $StartArea.connect("body_entered", self, "start_area_entered")
        $doorsatan.connect("body_entered", self, "door_entered")
    else:
        $Satan.connect("died", self, "satan_died")

    $AudioStreamPlayer.play()


func spawn_imp():
    var imp := ImpScene.instance()
    var direction = sign(rng.randf() - 0.25)
    imp.position = $CattyBoi.global_position + Vector2(direction * rng.randf_range(500.0, 1000.0), rng.randf_range(0.0, -1000.0))
    self.add_child(imp)
    ImpSpawnerTimer.start(MONSTER_SPAWN_TIME + rng.randf_range(-MONSTER_SPAWN_TIME / 2, +MONSTER_SPAWN_TIME / 2))


func level_transition():
    get_tree().change_scene("res://satanlevel.tscn")


func door_entered(other):
    if other != $CattyBoi:
        return

    $doorsatan/AnimationPlayer.play("leveltransition")


func start_area_entered(other):
    if other != $CattyBoi:
        return

    ImpSpawnerTimer.start(MONSTER_SPAWN_TIME)
    remove_child($StartArea)


func satan_died():
    $AudioStreamPlayer.stop()
    get_tree().create_timer(15.0).connect("timeout", self, "game_won")


func game_won():
    get_tree().change_scene("res://title.tscn")
