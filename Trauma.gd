extends Node

const HIT_TRAUMA_DECAY_FACTOR := 3.0
const HURT_TRAUMA_DECAY_FACTOR := 1.0

onready var _hit_trauma := 0.0
onready var _hurt_trauma := 0.0

func add_hurt_trauma(val: float):
    _hurt_trauma += val
    _hurt_trauma = clamp(_hurt_trauma, 0.0, 1.0)

func add_hit_trauma(val: float):
    _hit_trauma += val
    _hit_trauma = clamp(_hit_trauma, 0.0, 1.0)

func get_trauma() -> float:
    return min(_hurt_trauma + _hit_trauma, 1.0)

func _process(delta: float):
    add_hit_trauma(delta * -HIT_TRAUMA_DECAY_FACTOR)
    add_hurt_trauma(delta * -HURT_TRAUMA_DECAY_FACTOR)
