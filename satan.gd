extends KinematicBody2D


const FireballScene = preload("res://fireball.tscn")

signal died


const FRICTION := 0.9
const ACCEL := 750.0
const JUMP_SPEED = 2000.0
const GRAVITY = Vector2(0, 100)
const UP := Vector2(0, -1)
const FIREBALL_SPAWN_TIME := 2.0

onready var hit_points := 30
onready var velocity := Vector2.ZERO
onready var rng := RandomNumberGenerator.new()
var player = null


func _ready():
    self.add_to_group("Monsters")
    $FireballTimer.connect("timeout", self, "throw_fireball")
    $FireballTimer.start(FIREBALL_SPAWN_TIME)
    rng.randomize()


func _physics_process(delta : float):
    if player == null:
        player = get_tree().get_nodes_in_group("Player")[0]

    var movement := Vector2.ZERO
    self.velocity += GRAVITY
    if player != null and self.hit_points > 0:
        movement += player.global_position - self.global_position

    self.velocity = self.move_and_slide(
        (self.velocity * FRICTION) + (movement.normalized() * ACCEL * delta), UP)

    if self.global_position.y > 1000.0:
        hurt(1000)


func hurt(damage : int):
    if self.hit_points <= 0:
        return

    self.hit_points -= damage
    if self.hit_points <= 0:
        $FireballTimer.stop()
        emit_signal("died")
        $AudioDie.play()
        $AnimationPlayer.play("die")


func throw_fireball():
    if player == null:
        return

    var ball := FireballScene.instance()
    ball.position = $FireballSpawner.global_position
    $"..".add_child(ball)
    var distance_adjust = Vector2(-1.0, -1.0) * (($FireballSpawner.global_position.x - player.global_position.x) / 3.5)
    distance_adjust += Vector2(rng.randf_range(-100.0, 100.0), rng.randf_range(-100.0, 100.0))
    var ball_vector = (player.global_position + distance_adjust) - $FireballSpawner.global_position
    ball.look_at(player.global_position)
    ball.apply_central_impulse(ball_vector.normalized() * 2000.0)

