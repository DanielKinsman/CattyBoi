extends Control


func _input(event):
    if event.is_action_pressed("shoot") or event.is_action_pressed("ui_cancel") or event.is_action_pressed("ui_accept"):
        get_tree().change_scene("game.tscn")
