extends Control


func _input(event):
    if (event is InputEventKey or event is InputEventMouseButton
        or event is InputEventJoypadButton or event is InputEventScreenTouch):
        Input.set_mouse_mode(Input.MOUSE_MODE_CONFINED)
        get_tree().change_scene("res://game.tscn")

