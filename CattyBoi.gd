extends KinematicBody2D

var Bullets := preload("res://bullet.tscn")

const FRICTION := 0.9
const ACCEL := 3500.0
const JUMP_SPEED = 2000.0
const GRAVITY = Vector2(0, 100)
const UP := Vector2(0, -1)
const MAX_SHOTS := 2
const AXE_DAMAGE := 1
const AXE_KNOCKBACK := 2500.0

onready var velocity := Vector2.ZERO
onready var shots := 0
onready var hit_points := 3
onready var is_satan_level := false


func _ready():
    self.add_to_group("Player")
    $shoulder/axeshoulder/axe/AxeHurtBox.connect("body_entered", self, "_axe_entered")


func _physics_process(delta : float):
    var movement := process_input()
    self.velocity += GRAVITY

    self.velocity = self.move_and_slide(
        (self.velocity * FRICTION) + (movement.normalized() * ACCEL * delta), UP)

    if self.global_position.y > 1000.0:
        hurt(1000)


func process_input() -> Vector2:
    var movement := Vector2.ZERO

    if Input.is_action_just_pressed("ui_cancel"):
        if Input.get_mouse_mode() == Input.MOUSE_MODE_CONFINED:
            Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
        elif OS.get_name() != "HTML5":
            get_tree().quit()

        return movement

    if self.hit_points <= 0:
        return movement

    $shoulder.look_at($"..".get_local_mouse_position())
    $"../CameraDolly".target = $shoulder/gun.global_position + $shoulder/gun.global_transform.basis_xform(Vector2(300.0, 0.0))
    if is_satan_level:
        $"../CameraDolly".target += Vector2(0, -250.0)

    if Input.is_action_pressed("moveleft"):
        movement += Vector2(-1, 0)

    if Input.is_action_pressed("moveright"):
        movement += Vector2(1, 0)

    if Input.is_action_just_pressed("jump") and self.is_on_floor():
        self.velocity.y -= JUMP_SPEED

    if Input.is_action_just_pressed("shoot") and !$AnimationPlayer.is_playing():
        if shots <= 0:
            $shoulder/gun/outofbulletssound.play()
        else:
            shots -= 1
            var bulls := Bullets.instance()
            bulls.position = $shoulder/gun.global_transform.xform(Vector2(250.0, 0.0))
            $"..".add_child(bulls)
            var bullet_vector = $shoulder.transform.xform(Vector2(3000.0, 0.0))
            bulls.apply_central_impulse(bullet_vector)
            self.velocity -= bullet_vector / 3.0
            Trauma.add_hit_trauma(1.0)
            $"shoulder/gun/shot sound".play()
            $AnimationPlayer.play("shoot")

    if Input.is_action_just_pressed("axe") and !$AnimationPlayer.is_playing():
        $AnimationPlayer.play("axe")

    if Input.is_action_just_pressed("reload"):
        $AnimationPlayer.play("reload")
        $"shoulder/gun/reloadsound".play()
        shots = 2

    return movement

func hurt(damage : int):
    if self.hit_points <= 0:
        return

    self.hit_points -= damage
    $HurtSound.play()
    Trauma.add_hurt_trauma(damage / 2.0)
    if self.hit_points <= 0:
        Trauma.add_hurt_trauma(1.0)
        $AnimationPlayer.play("die")
        $GameOverSound.play()
        var gameovertimer = Timer.new()
        add_child(gameovertimer)
        gameovertimer.connect("timeout", self, "game_over")
        gameovertimer.start(3.0)


func _axe_entered(other):
    if other == self or !other.has_method("hurt"):
        return

    other.hurt(AXE_DAMAGE)
    var knockback := Vector2.ZERO
    knockback = (other.global_position - self.global_position).normalized() * AXE_KNOCKBACK
    other.velocity += knockback
    Trauma.add_hit_trauma(1.0)


func game_over():
    get_tree().change_scene("res://GameOver.tscn")


func _input(event):
    if event is InputEventMouseButton and Input.get_mouse_mode() != Input.MOUSE_MODE_CONFINED:
        Input.set_mouse_mode(Input.MOUSE_MODE_CONFINED)
